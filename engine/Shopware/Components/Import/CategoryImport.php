<?php

namespace Shopware\Components\Import;

class CategoryImport
{
    /**
     * @return array
     */
    public function getJsonDataInArray()
    {
        $jsonData = file_get_contents(__DIR__ . '/data.json');
        $arrayData = json_decode($jsonData, 1);
        return $arrayData;
    }

    /**
     * @param array $arrayData
     * @return array
     */
    public function getCategoriesFromArrayData(array $arrayData)
    {
        $data = array();
        $categories = array();
        foreach ($arrayData as $array) {
            if ($array['lang']) {
                $data['de'][] = $array['product_line_area'];
            } else {
                $data['en'][] = $array['product_line_area'];
            }
        }

        $categories['en'] = array_unique($data['en']);
        $categories['de'] = array_unique($data['de']);

        return $categories;
    }
}