<?php

namespace Shopware\Commands;

use Shopware\Components\Model\ModelManager;
use Shopware\Models\Category\Category;
use Shopware\Models\Category\Repository;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Shopware\Components\Import\CategoryImport;

class CategoryImportCommand extends ShopwareCommand
{
    /** @var SymfonyStyle */
    protected $io;

    /**
     * {@inheritdoc}
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->io->title('Import');
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('sw:category:import');
        $this->setDescription('Import Categories');

    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var ModelManager $em */
        $em = $this->container->get('models');
        /** @var Repository $categoryRepository */
        $categoryRepository = $em->getRepository(Category::class);
        /** @var CategoryImport $importManager */
        $importManager = $this->container->get('shopware.import.category_import');

        $arrayData = $importManager->getJsonDataInArray();
        $categories = $importManager->getCategoriesFromArrayData($arrayData);

        // Importing EN/DE Categories in Database
        $batchSize = 20;
        $i = 1;
        foreach ($categories as $category){
            if ($category['de']){
                //Supposed that "title" is relevant in DB with "name" (actually is "description" in Shopware Models)
                if (!$categoryRepository->findOneBy(['name' => $category['title'], 'lang' => 'de'])) {
                    $categoryObject = new Category();
                    $categoryObject->setName($category['title']);
                    $categoryObject->setLang('de');//Supposed to have a language column
                    $em->persist($categoryObject);
                    if (($i % $batchSize) === 0) {
                        $em->flush(); // Executes all updates.
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    ++$i;
                }
            } else {
                if (!$categoryRepository->findOneBy(['name' => $category['title'], 'lang' => 'en'])) {
                    $categoryObject = new Category();
                    $categoryObject->setName($category['title']);
                    $categoryObject->setLang('en');//Supposed to have a language column
                    $em->persist($categoryObject);
                    if (($i % $batchSize) === 0) {
                        $em->flush(); // Executes all updates.
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    ++$i;
                }
            }
        }
        $em->flush();
        $output->writeln('Categories are imported successfully.');
        // execute() method expect boolean value on return
        return true;
    }
}